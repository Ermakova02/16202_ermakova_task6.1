import ru.nsu.ccfit.ermakova.factory.Factory;

import javax.swing.*;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.LogManager;

public class FactoryApp {
    public static void main(String args[]) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        /*UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        JFrame.setDefaultLookAndFeelDecorated(true);
        JDialog.setDefaultLookAndFeelDecorated(true);*/
        try {
            LogManager.getLogManager().readConfiguration(
                    FactoryApp.class.getResourceAsStream("ru/nsu/ccfit/ermakova/factory/factory.properties"));
        } catch (IOException e) {
            System.err.println("Could not setup logger configuration: " + e.toString());
        }
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Factory();
            }
        });
    }
}
