package ru.nsu.ccfit.ermakova.factory;

public class Accessory extends Part {
    public Accessory() { super(); }

    public Accessory(Accessory accessory) { super(accessory); }

    public String toString() {
        String s = "Accessory (S/N: " + serial + ")";
        return s;
    }
}
