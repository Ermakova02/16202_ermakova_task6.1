package ru.nsu.ccfit.ermakova.factory;

import ru.nsu.ccfit.ermakova.threadpool.Operator;

public class AccessorySupplier extends Operator {
    private int supplierNum;
    private Factory factory;

    AccessorySupplier(String strName) {
        super(strName);
        supplierNum = 0;
        factory = null;
    }

    public void setSupplierNum(int num) { supplierNum = num; }

    public void setFactory(Factory f) { factory = f; }

    @Override
    protected void next() throws InterruptedException {
        if (started && !stopped && !paused) {
            String strSerial = String.format("A%03d%06d", supplierNum, factory.nextAccessorySerial());
            Accessory p = new Accessory();
            p.setSerial(strSerial);
            stg.put(p);
            count++;
            factory.setAccessoryWCount(stg.getElementsCount());
            factory.calculateProducedAccessories();
        }
    }
}
