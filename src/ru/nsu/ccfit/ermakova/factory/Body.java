package ru.nsu.ccfit.ermakova.factory;

public class Body extends Part {
    public Body() { super(); }

    public Body(Body body) { super(body); }

    public String toString() {
        String s = "Body (S/N: " + serial + ")";
        return s;
    }
}
