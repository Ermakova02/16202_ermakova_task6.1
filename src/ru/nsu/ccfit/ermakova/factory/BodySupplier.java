package ru.nsu.ccfit.ermakova.factory;

import ru.nsu.ccfit.ermakova.threadpool.Operator;


public class BodySupplier extends Operator {
    private Factory factory;

    BodySupplier(String strName) { super(strName); }

    public void setFactory(Factory f) { factory = f; }

    @Override
    protected void  next() throws InterruptedException {
        if (started && !stopped && !paused) {
            String strSerial = String.format("B%06d", factory.nextBodySerial());
            Body p = new Body();
            p.setSerial(strSerial);
            stg.put(p);
            count++;
            factory.setBodyWCount(stg.getElementsCount());
            factory.setProducedBodiesCount(count);
        }
    }
}
