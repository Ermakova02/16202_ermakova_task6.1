package ru.nsu.ccfit.ermakova.factory;

public class Car extends Part{
    private Body body;
    private Engine engine;
    private Accessory accessory;

    public Car(Body body, Engine engine, Accessory accessory) {
        this.body = new Body(body);
        this.engine = new Engine(engine);
        this.accessory = new Accessory(accessory);
    }

    public String getBodySerial() { return body.getSerial(); }

    public String getEngineSerial() { return engine.getSerial(); }

    public String getAccessorySerial() { return accessory.getSerial(); }

    public String toString() {
        String s = "Car (S/N: " + serial + ")";
        return s;
    }
}
