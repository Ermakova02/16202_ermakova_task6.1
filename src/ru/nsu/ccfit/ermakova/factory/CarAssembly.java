package ru.nsu.ccfit.ermakova.factory;

import ru.nsu.ccfit.ermakova.threadpool.Storage;

public class CarAssembly implements Runnable {
    private long speed;
    private Factory factory;
    protected Storage<Body> bodyWarehouse;
    protected Storage<Engine> engineWarehouse;
    protected Storage<Accessory> accessoryWarehouse;
    protected Storage<Car> carWarehouse;
    protected String name;

    public CarAssembly() {
        speed = 0;
        factory = null;
        engineWarehouse = null;
        bodyWarehouse = null;
        accessoryWarehouse = null;
        carWarehouse = null;
    }

    public void setFactory(Factory f) { factory = f; }

    public void setBodyWarehouse(Storage warehouse) { this.bodyWarehouse = warehouse; }

    public void setEngineWarehouse(Storage warehouse) { this.engineWarehouse = warehouse; }

    public void setAccessoryWarehouse(Storage warehouse) { this.accessoryWarehouse = warehouse; }

    public void setCarWarehouse(Storage warehouse) { this.carWarehouse = warehouse; }

    public void setName(String name) { this.name = name; }

    public void setSpeed(long speed) { this.speed = speed; }

    protected void assemblyCar() throws InterruptedException {
        factory.calculateWaitingWorkersCount();
        Body b = bodyWarehouse.get();
        Engine e = engineWarehouse.get();
        Accessory a = accessoryWarehouse.get();
        try {
            Thread.sleep(speed);
        } catch (InterruptedException e1) {
//            e1.printStackTrace();
        }
        Car c = new Car(b, e, a);
        String strSerial = String.format("C%06d", factory.nextCarSerial());
        c.setSerial(strSerial);
        carWarehouse.put(c);
        factory.increaseAssembledCarsCount();
        factory.setBodyWCount(bodyWarehouse.getElementsCount());
        factory.setEngineWCount(engineWarehouse.getElementsCount());
        factory.setAccessoryWCount(accessoryWarehouse.getElementsCount());
        factory.setCarWCount(carWarehouse.getElementsCount());
        factory.calculateWaitingWorkersCount();
        factory.tuneAssemblyTasks(-1);
    }

    @Override
    public void run() {
        try {
            assemblyCar();
        } catch (InterruptedException e) {
            // do nothing
        }
    }
}
