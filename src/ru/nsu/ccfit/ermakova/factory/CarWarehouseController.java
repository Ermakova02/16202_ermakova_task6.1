package ru.nsu.ccfit.ermakova.factory;

import java.util.concurrent.RejectedExecutionException;

public class CarWarehouseController {
    protected Factory factory;
    public CarWarehouseController(Factory f) { factory = f; }
    public void checkWarehouse() {
        if (!factory.isStarted()) return;
        int size = factory.getCarWarehouse().getSize();
        int num = factory.getCarWarehouse().getElementsCount();
        if (num >= size) return;
        CarAssembly workerTask = new CarAssembly();
        workerTask.setFactory(factory);
        workerTask.setBodyWarehouse(factory.getBodyWarehouse());
        workerTask.setEngineWarehouse(factory.getEngineWarehouse());
        workerTask.setAccessoryWarehouse(factory.getAccessoryWarehouse());
        workerTask.setCarWarehouse(factory.getCarWarehouse());
        workerTask.setSpeed(factory.getWorkerAssemblySpeed());
        workerTask.setName("000");
        factory.tuneAssemblyTasks(1);
        try {
            factory.getWorkersService().submitTask(workerTask);
        } catch (RejectedExecutionException e) {
//            e.printStackTrace();
        } catch (InterruptedException e) {
//            e.printStackTrace();
        }
//        factory.getWorkersService().execute(workerTask);
    }
}
