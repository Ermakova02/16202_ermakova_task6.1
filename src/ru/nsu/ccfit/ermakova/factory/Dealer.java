package ru.nsu.ccfit.ermakova.factory;

import ru.nsu.ccfit.ermakova.threadpool.*;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Dealer extends Operator {
    private static Logger log = Logger.getLogger(Dealer.class.getName());
    private CarWarehouseController controller;
    protected Factory factory;

    public Dealer(String strName) {
        super(strName);
        factory = null;
        controller = null;
    }

    public void setFactory(Factory f) {
        factory = f;
        controller = new CarWarehouseController(f);
    }

    @Override
    protected void next() throws InterruptedException {
        if (started && !stopped && !paused) {
            Car c = (Car) stg.get();
            factory.setCarWCount(stg.getElementsCount());
            factory.increaseSoldCarsCount();
            controller.checkWarehouse();
            String str = String.format("%s: %s: Auto %s (Body: %s, Motor: %s, Accessory: %s)",
                    new Date().toString(), name, c.getSerial(), c.getBodySerial(), c.getEngineSerial(), c.getAccessorySerial());
            if (factory.isLogSave())
                log.log(Level.INFO, str);
        }
    }
}
