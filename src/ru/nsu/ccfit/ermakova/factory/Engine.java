package ru.nsu.ccfit.ermakova.factory;

public class Engine extends Part {
    public Engine() {super(); }

    public Engine(Engine engine) {super(engine); }

    public String toString() {
        String s = "Engine (S/N: " + serial + ")";
        return s;
    }
}
