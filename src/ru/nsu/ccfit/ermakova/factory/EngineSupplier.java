package ru.nsu.ccfit.ermakova.factory;

import ru.nsu.ccfit.ermakova.threadpool.Operator;

public class EngineSupplier extends Operator {
    private Factory factory;

    EngineSupplier(String strName) {
        super(strName);
        factory = null;
    }

    public void setFactory(Factory f) { factory = f; }

    @Override
    protected  void next() throws InterruptedException {
        if (started && !stopped && !paused) {
            String strSerial = String.format("E%06d", factory.nextEngineSerial());
            Engine p = new Engine();
            p.setSerial(strSerial);
            stg.put(p);
            count++;
            factory.setEngineWCount(stg.getElementsCount());
            factory.setProducedEnginesCount(count);
        }
    }
}
