package ru.nsu.ccfit.ermakova.factory;

import ru.nsu.ccfit.ermakova.threadpool.Storage;
import ru.nsu.ccfit.ermakova.threadpool.ThreadPool;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.*;

public class Factory implements ActionListener {
    private static final String DEFAULT_FONT_NAME = "Segoe UI";
    private static final int DEFAULT_FONT_SIZE = 12;
    private static final String PROPERTIES_FILE_NAME = "ru/nsu/ccfit/ermakova/factory/factory.properties";

    public static final Dimension BUTTONS_SIZE = new Dimension(91, 26);
    public static final Dimension SETTINGS_DIALOG_SIZE = new Dimension(450, 350);

    public static final String SETTINGS_DIALOG_HEAD = "Settings";

    private JFrame frm;
    private Font defaultFont;
    private SettingsDialog settingsDlg;

    private int bodySerial;
    private int engineSerial;
    private int accessorySerial;
    private int carSerial;

    private int engineWCount;
    private int bodyWCount;
    private int accessoryWCount;
    private int carWCount;
    private int producedEnginesCount;
    private int producedBodiesCount;
    private int producedAccessoriesCount;
    private int producedCarsCount;
    private int waitingTasksCount;
    private int soldCarsCount;
    private int assembledCarsCount;
    private int activeAssemblyTasks;

    private BodySupplier bodySupplier;
    private EngineSupplier engineSupplier;
    private ArrayList<AccessorySupplier> accessorySuppliers;
    private ArrayList<Dealer> carDealers;

    private Storage<Body> bodyWarehouse;
    private Storage<Engine> engineWarehouse;
    private Storage<Accessory> accessoryWarehouse;
    private Storage<Car> carWarehouse;

    private ThreadPool workersService;
//    ExecutorService workersService;
//    ThreadPoolExecutor workersService;

    private int engineWarehouseSize;
    private int bodyWarehouseSize;
    private int accessoryWarehouseSize;
    private int carWarehouseSize;
    private int workers;
    private int dealers;
    private int accessories;
    private int engineSupplyTime;
    private int bodySupplyTime;
    private int accessorySupplyTime;
    private int workerAssemblyTime;
    private int dealerRequestTime;
    private boolean logSave;

    private boolean started;
    private boolean paused;

    private JButton startBtn;
    private JButton pauseBtn;
    private JButton stopBtn;
    private JButton settingsBtn;
    private JButton closeBtn;

    private FactoryUI ui;

    public Factory() {
        initDefaults();
        loadProperties();
        initElements();
        initListeners();
        frm.setVisible(true);
    }

    private void initDefaults() {
        settingsDlg = null;
        defaultFont = new Font(DEFAULT_FONT_NAME, Font.PLAIN, DEFAULT_FONT_SIZE);
        frm = new FactoryFrame();
        frm.setFont(defaultFont);
        workersService = null;
        bodySerial = 0;
        engineSerial = 0;
        accessorySerial = 0;
        carSerial = 0;
        engineWCount = 0;
        bodyWCount = 0;
        accessoryWCount = 0;
        carWCount = 0;
        producedEnginesCount = 0;
        producedBodiesCount = 0;
        producedAccessoriesCount = 0;
        producedCarsCount = 0;
        waitingTasksCount = 0;
        soldCarsCount = 0;
        assembledCarsCount = 0;
        activeAssemblyTasks = 0;
        started = false;
        paused = false;
        ui = new FactoryUI();
    }

    private void loadProperties() {
        Properties properties = new Properties();
        try {
            InputStream stream = getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME);
            properties.load(stream);
            engineWarehouseSize = Integer.parseInt(properties.getProperty("EngineWarehouseSize"));
            bodyWarehouseSize = Integer.parseInt(properties.getProperty("BodyWarehouseSize"));
            accessoryWarehouseSize = Integer.parseInt(properties.getProperty("AccessoryWarehouseSize"));
            carWarehouseSize = Integer.parseInt(properties.getProperty("CarWarehouseSize"));
            workers = Integer.parseInt(properties.getProperty("Workers"));
            dealers = Integer.parseInt(properties.getProperty("Dealers"));
            accessories = Integer.parseInt(properties.getProperty("AccessorySuppliers"));
            engineSupplyTime = Integer.parseInt(properties.getProperty("EngineSupplyTime"));
            bodySupplyTime = Integer.parseInt(properties.getProperty("BodySupplyTime"));
            accessorySupplyTime = Integer.parseInt(properties.getProperty("AccessorySupplyTime"));
            workerAssemblyTime = Integer.parseInt(properties.getProperty("WorkerAssemblyTime"));
            dealerRequestTime = Integer.parseInt(properties.getProperty("DealerRequestTime"));
            logSave = Boolean.parseBoolean(properties.getProperty("LogSave"));
        } catch (IOException e) {
            e.printStackTrace();
            frm.dispose();
            frm = null;
            System.exit(-1);
        }
    }

    private void initStaticElements() {
        ui.setEWSizeValueLabel(engineWarehouseSize);
        ui.setBWSizeValueLabel(bodyWarehouseSize);
        ui.setAWSizeValueLabel(accessoryWarehouseSize);
        ui.setCWSizeValueLabel(carWarehouseSize);
        ui.setCATimeValueLabel(workerAssemblyTime);
        ui.setCAWorkesNumberLabel(workers);
        ui.setDLNumberValueLabel(dealers);
        ui.setASNumberValueLabel(accessories);
        ui.setESTimeValueLabel(engineSupplyTime);
        ui.setBSTimeValueLabel(bodySupplyTime);
        ui.setASTimeValueLabel(accessorySupplyTime);
        ui.setDLDemandValueLabel(dealerRequestTime);
    }

    private void initElements() {
        startBtn = new JButton("Start");
        pauseBtn = new JButton("Pause");
        stopBtn = new JButton("Stop");
        settingsBtn = new JButton("Settings");
        closeBtn = new JButton("Close");

        startBtn.setPreferredSize(Factory.BUTTONS_SIZE);
        pauseBtn.setPreferredSize(Factory.BUTTONS_SIZE);
        stopBtn.setPreferredSize(Factory.BUTTONS_SIZE);
        settingsBtn.setPreferredSize(Factory.BUTTONS_SIZE);
        closeBtn.setPreferredSize(Factory.BUTTONS_SIZE);

        pauseBtn.setEnabled(false);
        stopBtn.setEnabled(false);

        ui.setMainContainer(frm.getContentPane());
        ui.addStartBtn(startBtn);
        ui.addPauseBtn(pauseBtn);
        ui.addStopBtn(stopBtn);
        ui.addSettingsBtn(settingsBtn);
        ui.addCloseBtn(closeBtn);

        initStaticElements();

        ui.createUI();
        frm.pack();
    }

    private void initListeners() {
        startBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                start();
            }
        });

        startBtn.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) start();
            }

        });

        pauseBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pause();
            }
        });

        pauseBtn.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) pause();
            }

        });

        stopBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stop();
            }
        });

        stopBtn.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) stop();
            }

        });

        settingsBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startSettingsDlg();
            }
        });

        settingsBtn.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) startSettingsDlg();
            }
        });

        closeBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                close();
            }
        });
        closeBtn.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) close();
            }
        });
        frm.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                close();
            }
        });
    }

    boolean isLogSave() { return logSave; }

    public synchronized void tuneAssemblyTasks(int tune) {
        activeAssemblyTasks += tune;
    }

    public int nextBodySerial() { bodySerial++; return bodySerial; }

    public int nextEngineSerial() { engineSerial++; return engineSerial; }

    public int nextAccessorySerial() { accessorySerial++; return accessorySerial; }

    public int nextCarSerial() { carSerial++; return carSerial; }

    public synchronized boolean isStarted() { return started; }

    public void start() {
        if (paused) {
            resume();
            return;
        }

        try {

            bodyWarehouse = new Storage<Body>();
            bodyWarehouse.setName("Body Storage");
            bodyWarehouse.setSize(bodyWarehouseSize);
            engineWarehouse = new Storage<Engine>();
            engineWarehouse.setName("Engine Storage");
            engineWarehouse.setSize(engineWarehouseSize);
            accessoryWarehouse = new Storage<Accessory>();
            accessoryWarehouse.setName("Accessory Storage");
            accessoryWarehouse.setSize(accessoryWarehouseSize);
            carWarehouse = new Storage<Car>();
            carWarehouse.setName("Car Storage");
            carWarehouse.setSize(carWarehouseSize);

            bodySupplier = new BodySupplier("Body Supplier");
            bodySupplier.setFactory(this);
            bodySupplier.setDelay(bodySupplyTime);
            bodySupplier.setStorage(bodyWarehouse);
            engineSupplier = new EngineSupplier("Engine Supplier");
            engineSupplier.setFactory(this);
            engineSupplier.setDelay(engineSupplyTime);
            engineSupplier.setStorage(engineWarehouse);
            soldCarsCount = 0;
            assembledCarsCount = 0;
            activeAssemblyTasks = 0;
            accessorySuppliers = new ArrayList<AccessorySupplier>(accessories);
            int i = 0;
            for (i = 0; i < accessories; i++) {
                int num = i + 1;
                AccessorySupplier s = new AccessorySupplier("Accessory Supplier " + num);
                s.setFactory(this);
                s.setDelay(accessorySupplyTime);
                s.setStorage(accessoryWarehouse);
                s.setSupplierNum(num);
                accessorySuppliers.add(s);
            }
            carDealers = new ArrayList<Dealer>(dealers);
            for (i = 0; i < dealers; i++) {
                int num = i + 1;
                Dealer d = new Dealer("Dealer " + num);
                d.setFactory(this);
                d.setDelay(dealerRequestTime);
                d.setStorage(carWarehouse);
                carDealers.add(d);
            }
            workersService = new ThreadPool(100, workers);
//        workersService = Executors.newFixedThreadPool(workers);
//        workersService = new ThreadPoolExecutor(workers, workers, 1, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<>(2));

            bodySupplier.start();
            engineSupplier.start();
            for (i = 0; i < accessories; i++) accessorySuppliers.get(i).start();
            for (i = 0; i < dealers; i++) carDealers.get(i).start();
            for (i = 0; i < carWarehouseSize; i++) {
                CarAssembly workerTask = new CarAssembly();
                workerTask.setFactory(this);
                workerTask.setBodyWarehouse(bodyWarehouse);
                workerTask.setEngineWarehouse(engineWarehouse);
                workerTask.setAccessoryWarehouse(accessoryWarehouse);
                workerTask.setCarWarehouse(carWarehouse);
                workerTask.setName(String.valueOf(i));
                workerTask.setSpeed(workerAssemblyTime);
                tuneAssemblyTasks(1);
                try {
                    workersService.submitTask(workerTask);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
//            workersService.submit(workerTask);
//            workersService.execute(workerTask);
            }
            started = true;

            startBtn.setEnabled(false);
            stopBtn.setEnabled(true);
            pauseBtn.setEnabled(true);
        }
        catch (InterruptedException e)
        {
            return;
        }
    }

    public void stop() {
        if (!started) return;
        started = false;
        paused = false;
        try {
            if (workersService != null)
                workersService.shutdown();
            if (bodySupplier != null) {
                bodySupplier.stop();
            }
            if (engineSupplier != null) {
                engineSupplier.stop();
            }
            int i = 0;
            if (accessorySuppliers != null) {
                for (i = 0; i < accessorySuppliers.size(); i++) {
                    AccessorySupplier s = accessorySuppliers.get(i);
                    if (s != null) s.stop();
                }
            }
            if (carDealers != null) {
                for (i = 0; i < carDealers.size(); i++) {
                    Dealer d = carDealers.get(i);
                    if (d != null) d.stop();
                }
            }
            bodySupplier.t.join();
            engineSupplier.t.join();
            if (accessorySuppliers != null) {
                for (i = 0; i < accessorySuppliers.size(); i++) {
                    AccessorySupplier s = accessorySuppliers.get(i);
                    if (s != null) s.t.join();
                }
            }
            if (carDealers != null) {
                for (i = 0; i < carDealers.size(); i++) {
                    Dealer d = carDealers.get(i);
                    if (d != null) d.t.join();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        bodySerial = 0;
        engineSerial = 0;
        accessorySerial = 0;
        carSerial = 0;

        activeAssemblyTasks = 0;

        startBtn.setEnabled(true);
        stopBtn.setEnabled(false);
        pauseBtn.setEnabled(false);

        ui.clearDynamicElements();
    }

    public void pause() {
        if (paused) return;
        if (workersService != null)
            workersService.shutdown();
        if (bodySupplier != null) bodySupplier.pause();
        if (engineSupplier != null) engineSupplier.pause();
        int i = 0;
        if (accessorySuppliers != null) {
            for (i = 0; i < accessorySuppliers.size(); i++) {
                AccessorySupplier s = accessorySuppliers.get(i);
                if (s != null) s.pause();
            }
        }
        if (carDealers != null) {
            for (i = 0; i < carDealers.size(); i++) {
                Dealer d = carDealers.get(i);
                if (d != null) d.pause();
            }
        }
        paused = true;
        activeAssemblyTasks = 0;

        startBtn.setEnabled(true);
        stopBtn.setEnabled(true);
        pauseBtn.setEnabled(false);
    }

    public void resume() {
        if (!paused) return;

        if (bodySupplier != null) bodySupplier.resume();
        if (engineSupplier != null) engineSupplier.resume();
        int i = 0;
        if (accessorySuppliers != null) {
            for (i = 0; i < accessorySuppliers.size(); i++) {
                AccessorySupplier s = accessorySuppliers.get(i);
                if (s != null) s.resume();
            }
        }
        if (carDealers != null) {
            for (i = 0; i < carDealers.size(); i++) {
                Dealer d = carDealers.get(i);
                if (d != null) d.resume();
            }
        }

        if (workersService != null)
            workersService.shutdown();
        workersService = new ThreadPool(100, workers);
//        workersService = Executors.newFixedThreadPool(workers);
//        workersService = new ThreadPoolExecutor(workers, workers, 1, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<>(2));

        for (i = 0; i < carWarehouseSize; i++) {
            CarAssembly workerTask = new CarAssembly();
            workerTask.setFactory(this);
            workerTask.setBodyWarehouse(bodyWarehouse);
            workerTask.setEngineWarehouse(engineWarehouse);
            workerTask.setAccessoryWarehouse(accessoryWarehouse);
            workerTask.setCarWarehouse(carWarehouse);
            workerTask.setName(String.valueOf(i));
            workerTask.setSpeed(workerAssemblyTime);
            tuneAssemblyTasks(1);
            try {
                workersService.submitTask(workerTask);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//            workersService.execute(workerTask);
        }

        paused = false;

        startBtn.setEnabled(false);
        stopBtn.setEnabled(true);
        pauseBtn.setEnabled(true);
    }

    private void close() {
        startBtn.setEnabled(false);
        stopBtn.setEnabled(false);
        pauseBtn.setEnabled(false);
        stop();
        frm.dispose();
        System.exit(0);
    }

    public void updateSettings(boolean yes) {
        if (yes) {
            boolean wasStarted = started;
            if (started) stop();
            engineSupplyTime = settingsDlg.getESTimeValue();
            bodySupplyTime = settingsDlg.getBSTimeValue();
            accessorySupplyTime = settingsDlg.getASTimeValue();
            accessories = settingsDlg.getASNumberValue();
            dealerRequestTime = settingsDlg.getDLTimeValue();
            dealers = settingsDlg.getDLNumberValue();
            workerAssemblyTime = settingsDlg.getWTimeValue();
            workers = settingsDlg.getWNumberValue();
            initStaticElements();
            ui.updateUI();
            if (wasStarted) start();
        } else {
            if (started) resume();
        }
    }

    public Storage<Body> getBodyWarehouse() { return bodyWarehouse; }

    public Storage<Engine> getEngineWarehouse() { return engineWarehouse; }

    public Storage<Accessory> getAccessoryWarehouse() { return accessoryWarehouse; }

    public Storage<Car> getCarWarehouse() { return carWarehouse; }

    public int getWorkerAssemblySpeed() { return workerAssemblyTime; }

//    public ExecutorService getWorkersService() { return workersService; }

    public ThreadPool getWorkersService() { return workersService; }

    public void startSettingsDlg() {
        pause();
        if (settingsDlg == null)
            settingsDlg = new SettingsDialog(frm, this);
        settingsDlg.setESTimeValue(engineSupplyTime);
        settingsDlg.setBSTimeValue(bodySupplyTime);
        settingsDlg.setASTimeValue(accessorySupplyTime);
        settingsDlg.setASNumberValue(accessories);
        settingsDlg.setDLTimeValue(dealerRequestTime);
        settingsDlg.setDLNumberValue(dealers);
        settingsDlg.setWTimeValue(workerAssemblyTime);
        settingsDlg.setWNumberValue(workers);
        settingsDlg.setVisible(true);
    }

    public synchronized void setBodyWCount(int cnt) {
        bodyWCount = cnt;
        ui.setBWCountValueLabel(bodyWCount);
    }

    public synchronized void setEngineWCount(int cnt) {
        engineWCount = cnt;
        ui.setEWCountValueLabel(engineWCount);
    }

    public synchronized void setAccessoryWCount(int cnt) {
        accessoryWCount = cnt;
        ui.setAWCountValueLabel(accessoryWCount);
    }

    public synchronized void setCarWCount(int cnt) {
        carWCount = cnt;
        ui.setCWCountValueLabel(carWCount);
    }

    public synchronized void setProducedEnginesCount(int cnt) {
        producedEnginesCount = cnt;
        ui.setESProducedValueLabel(producedEnginesCount);
    }

    public synchronized void setProducedBodiesCount(int cnt) {
        producedBodiesCount = cnt;
        ui.setBSProducedValueLabel(producedBodiesCount);
    }

    public synchronized void setProducedAccessoriesCount(int cnt) {
        producedAccessoriesCount = cnt;
        ui.setASProducedValueLabel(producedAccessoriesCount);
    }

    public synchronized void setProducedCarsCount(int cnt) {
        producedCarsCount = cnt;
        ui.setCAAssembledNumberLabel(producedCarsCount);
    }

    public synchronized void setWaitingTasksCount(int cnt) {
        waitingTasksCount = cnt;
        ui.setCAWaitingNumberLabel(waitingTasksCount);
    }

    public synchronized void setSoldCarsCount(int cnt) {
        soldCarsCount = cnt;
        ui.setDLSoldValueLabel(soldCarsCount);
    }

    public void calculateProducedAccessories() {
        int cnt = 0;
        for (int i = 0; i < accessories; i++) cnt += accessorySuppliers.get(i).getCount();
        setProducedAccessoriesCount(cnt);
    }

    public synchronized void calculateWaitingWorkersCount() {
        /*int active = workersService.getActiveCount();
        waitingTasksCount = workers < active ? 0 : workers - active;
        System.out.println(
            String.format("Pool: %d, CorePool: %d, Active: %d, Completed: %d, Task: %d, isShutdown: %s, isTerminated: %s, FREE WORKERS: %d",
                workersService.getPoolSize(),
                workersService.getCorePoolSize(),
                workersService.getActiveCount(),
                workersService.getCompletedTaskCount(),
                workersService.getTaskCount(),
                workersService.isShutdown(),
                workersService.isTerminated(),
                waitingTasksCount));*/
        /*waitingTasksCount = activeAssemblyTasks < workers ? 0 : activeAssemblyTasks - workers;
        setWaitingTasksCount(waitingTasksCount);*/
        setWaitingTasksCount(workersService.waitingTasksCount());
    }

    synchronized public void increaseAssembledCarsCount() {
        assembledCarsCount++;
        ui.setCAAssembledNumberLabel(assembledCarsCount);
    }

    synchronized public void increaseSoldCarsCount() {
        soldCarsCount++;
        ui.setDLSoldValueLabel(soldCarsCount);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
