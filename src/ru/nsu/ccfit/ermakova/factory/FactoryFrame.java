package ru.nsu.ccfit.ermakova.factory;

import javax.swing.*;
import java.awt.*;

public class FactoryFrame extends JFrame {
    public static final String MAIN_FRAME_HEAD_TEXT = "Factory";
    public static final String MAIN_FRAME_HEAD_PAUSED_TEXT = "Factory (paused)";
    public static final Dimension MAIN_FRAME_SIZE = new Dimension(850, 550);

    public static final String MAIN_FRAME_ICON = "icons/main.png";

    public FactoryFrame() {
        setTitle(MAIN_FRAME_HEAD_TEXT);
        setSize(MAIN_FRAME_SIZE);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(MAIN_FRAME_ICON)));
//        setLocationRelativeTo(null);
        setLocationByPlatform(true);
        setResizable(false);
    }
}
