package ru.nsu.ccfit.ermakova.factory;

import javax.swing.*;
import java.awt.*;

public class FactoryUI {
    private static final String DEFAULT_FONT_NAME = "Segoe UI";
    private static final int DEFAULT_FONT_SIZE = 12;

    private JPanel infoPanel;
    private JPanel buttonsPanel;

    private JLabel esNameLabel;
    private JLabel esTimeNameLabel;
    private JLabel esTimeValueLabel;
    private JLabel esProducedNameLabel;
    private JLabel esProducedValueLabel;

    private JLabel bsNameLabel;
    private JLabel bsTimeNameLabel;
    private JLabel bsTimeValueLabel;
    private JLabel bsProducedNameLabel;
    private JLabel bsProducedValueLabel;

    private JLabel asNameLabel;
    private JLabel asTimeNameLabel;
    private JLabel asTimeValueLabel;
    private JLabel asNumberNameLabel;
    private JLabel asNumberValueLabel;
    private JLabel asProducedNameLabel;
    private JLabel asProducedValueLabel;

    private JLabel ewNameLabel;
    private JLabel ewSizeNameLabel;
    private JLabel ewSizeValueLabel;
    private JLabel ewCountNameLabel;
    private JLabel ewCountValueLabel;

    private JLabel bwNameLabel;
    private JLabel bwSizeNameLabel;
    private JLabel bwSizeValueLabel;
    private JLabel bwCountNameLabel;
    private JLabel bwCountValueLabel;

    private JLabel awNameLabel;
    private JLabel awSizeNameLabel;
    private JLabel awSizeValueLabel;
    private JLabel awCountNameLabel;
    private JLabel awCountValueLabel;

    private JLabel caNameLabel;
    private JLabel caWorkesNameLabel;
    private JLabel caWorkesNumberLabel;
    private JLabel caWaitingNameLabel;
    private JLabel caWaitingNumberLabel;
    private JLabel caTimeNameLabel;
    private JLabel caTimeNumberLabel;
    private JLabel caAssembledNameLabel;
    private JLabel caAssembledNumberLabel;

    private JLabel cwNameLabel;
    private JLabel cwSizeNameLabel;
    private JLabel cwSizeValueLabel;
    private JLabel cwCountNameLabel;
    private JLabel cwCountValueLabel;

    private JLabel dlNameLabel;
    private JLabel dlNumberNameLabel;
    private JLabel dlNumberValueLabel;
    private JLabel dlDemandNameLabel;
    private JLabel dlDemandValueLabel;
    private JLabel dlSoldNameLabel;
    private JLabel dlSoldValueLabel;

    private JButton startBtn;
    private JButton pauseBtn;
    private JButton stopBtn;
    private JButton settingsBtn;
    private JButton closeBtn;

    Container mainContainer;

    private Font defaultFont;
    private Font defaultFontBold;

    FactoryUI() {
        defaultFont = new Font(DEFAULT_FONT_NAME, Font.PLAIN, DEFAULT_FONT_SIZE);
        defaultFontBold = new Font(DEFAULT_FONT_NAME, Font.BOLD, DEFAULT_FONT_SIZE);

        mainContainer = null;
        infoPanel = new JPanel(new GridLayout(12, 6, 5, 5));
        buttonsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

        esNameLabel = new JLabel("Engine Supplier");
        esNameLabel.setFont(defaultFontBold);
        esTimeNameLabel = new JLabel("Supply Time:");
        esTimeNameLabel.setFont(defaultFont);
        esTimeValueLabel = new JLabel("");
        esTimeValueLabel.setFont(defaultFontBold);
        esTimeValueLabel.setForeground(new Color(0, 196, 0));
        esProducedNameLabel = new JLabel("Parts Produced:");
        esProducedNameLabel.setFont(defaultFont);
        esProducedValueLabel = new JLabel("");
        esProducedValueLabel.setFont(defaultFontBold);
        esProducedValueLabel.setForeground(Color.RED);

        bsNameLabel = new JLabel("Body Supplier");
        bsNameLabel.setFont(defaultFontBold);
        bsTimeNameLabel = new JLabel("Supply Time:");
        bsTimeNameLabel.setFont(defaultFont);
        bsTimeValueLabel = new JLabel("");
        bsTimeValueLabel.setFont(defaultFontBold);
        bsTimeValueLabel.setForeground(new Color(0, 196, 0));
        bsProducedNameLabel = new JLabel("Parts Produced:");
        bsProducedNameLabel.setFont(defaultFont);
        bsProducedValueLabel = new JLabel("");
        bsProducedValueLabel.setFont(defaultFontBold);
        bsProducedValueLabel.setForeground(Color.RED);

        asNameLabel = new JLabel("Accessory Suppliers");;
        asNameLabel.setFont(defaultFontBold);
        asTimeNameLabel = new JLabel("Supply Time:");
        asTimeNameLabel.setFont(defaultFont);
        asTimeValueLabel = new JLabel("");
        asTimeValueLabel.setFont(defaultFontBold);
        asTimeValueLabel.setForeground(new Color(0, 196, 0));
        asNumberNameLabel = new JLabel("Suppliers Number:");;
        asNumberNameLabel.setFont(defaultFont);
        asNumberValueLabel = new JLabel("");
        asNumberValueLabel.setFont(defaultFontBold);
        asNumberValueLabel.setForeground(new Color(0, 196, 0));
        asProducedNameLabel = new JLabel("Parts Produced:");
        asProducedNameLabel.setFont(defaultFont);
        asProducedValueLabel = new JLabel("");
        asProducedValueLabel.setFont(defaultFontBold);
        asProducedValueLabel.setForeground(Color.RED);

        ewNameLabel = new JLabel("Engine Storage");
        ewNameLabel.setFont(defaultFontBold);
        ewSizeNameLabel = new JLabel("Storage Size:");
        ewSizeNameLabel.setFont(defaultFont);
        ewSizeValueLabel = new JLabel("");
        ewSizeValueLabel.setFont(defaultFontBold);
        ewSizeValueLabel.setForeground(new Color(0, 196, 0));
        ewCountNameLabel = new JLabel("Parts Number:");
        ewCountNameLabel.setFont(defaultFont);
        ewCountValueLabel = new JLabel("");
        ewCountValueLabel.setFont(defaultFontBold);
        ewCountValueLabel.setForeground(Color.RED);

        bwNameLabel = new JLabel("Body Storage");
        bwNameLabel.setFont(defaultFontBold);
        bwSizeNameLabel = new JLabel("Storage Size:");
        bwSizeNameLabel.setFont(defaultFont);
        bwSizeValueLabel = new JLabel("");
        bwSizeValueLabel.setFont(defaultFontBold);
        bwSizeValueLabel.setForeground(new Color(0, 196, 0));
        bwCountNameLabel = new JLabel("Parts Number:");;
        bwCountNameLabel.setFont(defaultFont);
        bwCountValueLabel = new JLabel("");;
        bwCountValueLabel.setFont(defaultFontBold);
        bwCountValueLabel.setForeground(Color.RED);

        awNameLabel = new JLabel("Accessory Storage");
        awNameLabel.setFont(defaultFontBold);
        awSizeNameLabel = new JLabel("Storage Size:");
        awSizeNameLabel.setFont(defaultFont);
        awSizeValueLabel = new JLabel("");
        awSizeValueLabel.setFont(defaultFontBold);
        awSizeValueLabel.setForeground(new Color(0, 196, 0));
        awCountNameLabel = new JLabel("Parts Number:");;
        awCountNameLabel.setFont(defaultFont);
        awCountValueLabel = new JLabel("");;
        awCountValueLabel.setFont(defaultFontBold);
        awCountValueLabel.setForeground(Color.RED);

        caNameLabel = new JLabel("Car Assembly");
        caNameLabel.setFont(defaultFontBold);
        caWorkesNameLabel = new JLabel("Workers Number:");
        caWorkesNameLabel.setFont(defaultFont);
        caWorkesNumberLabel = new JLabel("");
        caWorkesNumberLabel.setForeground(new Color(0, 196, 0));
        caWorkesNumberLabel.setFont(defaultFontBold);
        caTimeNameLabel = new JLabel("Assembly Time:");
        caTimeNameLabel.setFont(defaultFont);
        caTimeNumberLabel = new JLabel("");
        caTimeNumberLabel.setForeground(new Color(0, 196, 0));
        caTimeNumberLabel.setFont(defaultFontBold);
        caWaitingNameLabel = new JLabel("Tasks Waiting: ");
        caWaitingNameLabel.setFont(defaultFont);
        caWaitingNumberLabel = new JLabel("");
        caWaitingNumberLabel.setFont(defaultFontBold);
        caWaitingNumberLabel.setForeground(Color.RED);
        caAssembledNameLabel = new JLabel("Cars Assembled:");
        caAssembledNameLabel.setFont(defaultFont);
        caAssembledNumberLabel = new JLabel("");
        caAssembledNumberLabel.setFont(defaultFontBold);
        caAssembledNumberLabel.setForeground(Color.RED);

        cwNameLabel = new JLabel("Cars Storage");
        cwNameLabel.setFont(defaultFontBold);
        cwSizeNameLabel = new JLabel("Storage Size:");
        cwSizeNameLabel.setFont(defaultFont);
        cwSizeValueLabel = new JLabel("");
        cwSizeValueLabel.setFont(defaultFontBold);
        cwSizeValueLabel.setForeground(new Color(0, 196, 0));
        cwCountNameLabel = new JLabel("Cars Number:");;
        cwCountNameLabel.setFont(defaultFont);
        cwCountValueLabel = new JLabel("");;
        cwCountValueLabel.setFont(defaultFontBold);
        cwCountValueLabel.setForeground(Color.RED);

        dlNameLabel = new JLabel("Dealers");
        dlNameLabel.setFont(defaultFontBold);
        dlNumberNameLabel = new JLabel("Dealers Number:");
        dlNumberNameLabel.setFont(defaultFont);
        dlNumberValueLabel = new JLabel("");
        dlNumberValueLabel.setFont(defaultFontBold);
        dlNumberValueLabel.setForeground(new Color(0, 196, 0));
        dlDemandNameLabel = new JLabel("Demand Time:");;
        dlDemandNameLabel.setFont(defaultFont);
        dlDemandValueLabel = new JLabel("");
        dlDemandValueLabel.setFont(defaultFontBold);
        dlDemandValueLabel.setForeground(new Color(0, 196, 0));
        dlSoldNameLabel = new JLabel("Cars Sold:");
        dlSoldNameLabel.setFont(defaultFont);
        dlSoldValueLabel = new JLabel("");
        dlSoldValueLabel.setFont(defaultFontBold);
        dlSoldValueLabel.setForeground(Color.RED);

        startBtn = new JButton();
        pauseBtn = new JButton();
        stopBtn = new JButton();
        settingsBtn = new JButton();
        closeBtn = new JButton();
    }

    public void createUI() {
        mainContainer.setLayout(new BorderLayout());
        mainContainer.add(infoPanel, BorderLayout.CENTER);
        mainContainer.add(buttonsPanel, BorderLayout.SOUTH);

        // Line 1
        infoPanel.add(esNameLabel);
        infoPanel.add(new JLabel(""));

        infoPanel.add(ewNameLabel);
        infoPanel.add(new JLabel(""));

        infoPanel.add(caNameLabel);
        infoPanel.add(new JLabel(""));

        // Line 2
        infoPanel.add(esTimeNameLabel);
        infoPanel.add(esTimeValueLabel);

        infoPanel.add(ewSizeNameLabel);
        infoPanel.add(ewSizeValueLabel);

        infoPanel.add(caWorkesNameLabel);
        infoPanel.add(caWorkesNumberLabel);

        // Line 3
        infoPanel.add(esProducedNameLabel);
        infoPanel.add(esProducedValueLabel);

        infoPanel.add(ewCountNameLabel);
        infoPanel.add(ewCountValueLabel);

        infoPanel.add(caTimeNameLabel);
        infoPanel.add(caTimeNumberLabel);

        // Line 4
        infoPanel.add(bsNameLabel);
        infoPanel.add(new JLabel(""));

        infoPanel.add(bwNameLabel);
        infoPanel.add(new JLabel(""));

        infoPanel.add(caWaitingNameLabel);
        infoPanel.add(caWaitingNumberLabel);

        // Line 5
        infoPanel.add(bsTimeNameLabel);
        infoPanel.add(bsTimeValueLabel);

        infoPanel.add(bwSizeNameLabel);
        infoPanel.add(bwSizeValueLabel);

        infoPanel.add(caAssembledNameLabel);
        infoPanel.add(caAssembledNumberLabel);

        // Line 6
        infoPanel.add(bsProducedNameLabel);
        infoPanel.add(bsProducedValueLabel);

        infoPanel.add(bwCountNameLabel);
        infoPanel.add(bwCountValueLabel);

        infoPanel.add(cwNameLabel);
        infoPanel.add(new JLabel(""));

        // Line 7
        infoPanel.add(asNameLabel);
        infoPanel.add(new JLabel(""));

        infoPanel.add(awNameLabel);
        infoPanel.add(new JLabel(""));

        infoPanel.add(cwSizeNameLabel);
        infoPanel.add(cwSizeValueLabel);

        // Line 8
        infoPanel.add(asTimeNameLabel);
        infoPanel.add(asTimeValueLabel);

        infoPanel.add(awSizeNameLabel);
        infoPanel.add(awSizeValueLabel);

        infoPanel.add(cwCountNameLabel);
        infoPanel.add(cwCountValueLabel);

        // Line 9
        infoPanel.add(asNumberNameLabel);
        infoPanel.add(asNumberValueLabel);

        infoPanel.add(awCountNameLabel);
        infoPanel.add(awCountValueLabel);

        infoPanel.add(dlNameLabel);
        infoPanel.add(new JLabel(""));

        // Line 10
        infoPanel.add(asProducedNameLabel);
        infoPanel.add(asProducedValueLabel);

        infoPanel.add(new JLabel(""));
        infoPanel.add(new JLabel(""));

        infoPanel.add(dlNumberNameLabel);
        infoPanel.add(dlNumberValueLabel);

        // Line 10
        infoPanel.add(new JLabel(""));
        infoPanel.add(new JLabel(""));

        infoPanel.add(new JLabel(""));
        infoPanel.add(new JLabel(""));

        infoPanel.add(dlDemandNameLabel);
        infoPanel.add(dlDemandValueLabel);

        // Line 11
        infoPanel.add(new JLabel(""));
        infoPanel.add(new JLabel(""));

        infoPanel.add(new JLabel(""));
        infoPanel.add(new JLabel(""));

        infoPanel.add(dlSoldNameLabel);
        infoPanel.add(dlSoldValueLabel);

        buttonsPanel.add(startBtn);
        buttonsPanel.add(pauseBtn);
        buttonsPanel.add(stopBtn);
        buttonsPanel.add(settingsBtn);
        buttonsPanel.add(closeBtn);
    }

    public void setMainContainer(Container container) { mainContainer = container; }
    public void updateUI() { infoPanel.updateUI(); }

    public void addStartBtn(JButton btn) { startBtn = btn; }
    public void addPauseBtn(JButton btn) { pauseBtn = btn; }
    public void addStopBtn(JButton btn) { stopBtn = btn; }
    public void addSettingsBtn(JButton btn) { settingsBtn = btn; }
    public void addCloseBtn(JButton btn) { closeBtn = btn; }

    public void clearDynamicElements() {
        esProducedValueLabel.setText("");
        bsProducedValueLabel.setText("");
        asProducedValueLabel.setText("");
        ewCountValueLabel.setText("");
        bwCountValueLabel.setText("");
        awCountValueLabel.setText("");
        caWaitingNumberLabel.setText("");
        caAssembledNumberLabel.setText("");
        cwCountValueLabel.setText("");
        dlSoldValueLabel.setText("");
        updateUI();
    }

    synchronized public void setESTimeValueLabel(int val) { esTimeValueLabel.setText(String.valueOf(val)); esTimeValueLabel.updateUI(); }
    synchronized public void setESProducedValueLabel(int val) { esProducedValueLabel.setText(String.valueOf(val)); esProducedValueLabel.updateUI(); }
    synchronized public void setBSTimeValueLabel(int val) { bsTimeValueLabel.setText(String.valueOf(val)); bsTimeValueLabel.updateUI(); }
    synchronized public void setBSProducedValueLabel(int val) { bsProducedValueLabel.setText(String.valueOf(val)); bsProducedValueLabel.updateUI(); }
    synchronized public void setASTimeValueLabel(int val) { asTimeValueLabel.setText(String.valueOf(val)); asTimeValueLabel.updateUI(); }
    synchronized public void setASNumberValueLabel(int val) { asNumberValueLabel.setText(String.valueOf(val)); asNumberValueLabel.updateUI(); }
    synchronized public void setASProducedValueLabel(int val) { asProducedValueLabel.setText(String.valueOf(val)); asProducedValueLabel.updateUI(); }
    synchronized public void setEWSizeValueLabel(int val) { ewSizeValueLabel.setText(String.valueOf(val)); ewSizeValueLabel.updateUI(); }
    synchronized public void setEWCountValueLabel(int val) { ewCountValueLabel.setText(String.valueOf(val)); ewCountValueLabel.updateUI(); }
    synchronized public void setBWSizeValueLabel(int val) { bwSizeValueLabel.setText(String.valueOf(val)); bwSizeValueLabel.updateUI(); }
    synchronized public void setBWCountValueLabel(int val) { bwCountValueLabel.setText(String.valueOf(val)); bwCountValueLabel.updateUI(); }
    synchronized public void setAWSizeValueLabel(int val) { awSizeValueLabel.setText(String.valueOf(val)); awSizeValueLabel.updateUI(); }
    synchronized public void setAWCountValueLabel(int val) { awCountValueLabel.setText(String.valueOf(val)); awCountValueLabel.updateUI(); }
    synchronized public void setCATimeValueLabel(int val) { caTimeNumberLabel.setText(String.valueOf(val)); caTimeNumberLabel.updateUI(); }
    synchronized public void setCAWorkesNumberLabel(int val) { caWorkesNumberLabel.setText(String.valueOf(val)); caWorkesNumberLabel.updateUI(); }
    synchronized public void setCAWaitingNumberLabel(int val) { caWaitingNumberLabel.setText(String.valueOf(val)); caWaitingNumberLabel.updateUI(); }
    synchronized public void setCAAssembledNumberLabel(int val) { caAssembledNumberLabel.setText(String.valueOf(val)); caAssembledNumberLabel.updateUI(); }
    synchronized public void setCWSizeValueLabel(int val) { cwSizeValueLabel.setText(String.valueOf(val)); cwSizeValueLabel.updateUI(); }
    synchronized public void setCWCountValueLabel(int val) { cwCountValueLabel.setText(String.valueOf(val)); cwCountValueLabel.updateUI(); }
    synchronized public void setDLNumberValueLabel(int val) { dlNumberValueLabel.setText(String.valueOf(val)); dlNumberValueLabel.updateUI(); }
    synchronized public void setDLDemandValueLabel(int val) { dlDemandValueLabel.setText(String.valueOf(val)); dlDemandValueLabel.updateUI(); }
    synchronized public void setDLSoldValueLabel(int val) { dlSoldValueLabel.setText(String.valueOf(val)); dlSoldValueLabel.updateUI(); }
}
