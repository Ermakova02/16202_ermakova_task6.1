package ru.nsu.ccfit.ermakova.factory;

public class Part {
    protected String serial;

    public void setSerial(String serial) { this.serial = serial; }

    public String getSerial() { return serial; }

    public Part() { serial = ""; }

    public Part(Part part) { serial = new String(part.serial); }
}
