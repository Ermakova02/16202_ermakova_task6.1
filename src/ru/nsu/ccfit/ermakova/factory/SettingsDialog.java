package ru.nsu.ccfit.ermakova.factory;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;

public class SettingsDialog extends JDialog {
    private static final String DEFAULT_FONT_NAME = "Segoe UI";
    private static final int DEFAULT_FONT_SIZE = 12;

    Factory factory;
    JFrame owner;
    JButton okBtn;
    JButton cancelBtn;
    Container mainContainer;

    private Font defaultFont;
    private Font defaultFontBold;

    private JPanel infoPanel;
    private JPanel buttonsPanel;

    private JLabel esNameLabel;
    private JLabel esTimeValueLabel;
    private JLabel esTimeNameLabel;
    private JSlider esTimeValueSlider;

    private JLabel bsNameLabel;
    private JLabel bsTimeValueLabel;
    private JLabel bsTimeNameLabel;
    private JSlider bsTimeValueSlider;

    private JLabel asNameLabel;
    private JLabel asTimeValueLabel;
    private JLabel asTimeNameLabel;
    private JSlider asTimeValueSlider;
    private JLabel asNumberNameLabel;
    private JTextField asNumberValueText;

    private JLabel wNameLabel;
    private JLabel wTimeValueLabel;
    private JLabel wTimeNameLabel;
    private JSlider wTimeValueSlider;
    private JLabel wNumberNameLabel;
    private JTextField wNumberValueText;

    private JLabel dlNameLabel;
    private JLabel dlTimeValueLabel;
    private JLabel dlTimeNameLabel;
    private JSlider dlTimeValueSlider;
    private JLabel dlNumberNameLabel;
    private JTextField dlNumberValueText;

    private void initElements() {
        defaultFont = new Font(DEFAULT_FONT_NAME, Font.PLAIN, DEFAULT_FONT_SIZE);
        defaultFontBold = new Font(DEFAULT_FONT_NAME, Font.BOLD, DEFAULT_FONT_SIZE);

        mainContainer = this.getContentPane();
        infoPanel = new JPanel(new GridLayout(7, 4, 5, 5));
        buttonsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

        esNameLabel = new JLabel("Engine Supplier");
        esNameLabel.setFont(defaultFontBold);
        esTimeValueLabel = new JLabel("1000");
        esTimeValueLabel.setFont(defaultFontBold);
        esTimeNameLabel = new JLabel("Supply Time:");
        esTimeNameLabel.setFont(defaultFont);
        esTimeValueSlider = new JSlider(10, 5000, 1000);
        esTimeValueSlider.setPaintTicks(true);

        bsNameLabel = new JLabel("Body Supplier");
        bsNameLabel.setFont(defaultFontBold);
        bsTimeValueLabel = new JLabel("1000");
        bsTimeValueLabel.setFont(defaultFontBold);
        bsTimeNameLabel = new JLabel("Supply Time:");
        bsTimeNameLabel.setFont(defaultFont);
        bsTimeValueSlider = new JSlider(10, 5000, 1000);

        asNameLabel = new JLabel("Accessory Suppliers");
        asTimeValueLabel = new JLabel("1000");
        asTimeValueLabel.setFont(defaultFontBold);
        asNameLabel.setFont(defaultFontBold);
        asTimeNameLabel = new JLabel("Supply Time:");
        asTimeNameLabel.setFont(defaultFont);
        asTimeValueSlider = new JSlider(10, 5000, 1000);
        asNumberNameLabel = new JLabel("Suppliers Number:");
        asNumberNameLabel.setFont(defaultFont);
        asNumberValueText = new JTextField("3", 5);
        asNumberValueText.setFont(defaultFont);

        wNameLabel = new JLabel("Workesr");
        wTimeValueLabel = new JLabel("1000");
        wTimeValueLabel.setFont(defaultFontBold);
        wNameLabel.setFont(defaultFontBold);
        wTimeNameLabel = new JLabel("Assembly Time:");
        wTimeNameLabel.setFont(defaultFont);
        wTimeValueSlider = new JSlider(10, 5000, 1000);
        wNumberNameLabel = new JLabel("Workers Number:");
        wNumberNameLabel.setFont(defaultFont);
        wNumberValueText = new JTextField("3", 5);
        wNumberValueText.setFont(defaultFont);

        dlNameLabel = new JLabel("Dealers");
        dlNameLabel.setFont(defaultFontBold);
        dlTimeValueLabel = new JLabel("1000");
        dlTimeValueLabel.setFont(defaultFontBold);
        dlTimeNameLabel = new JLabel("Demand Time:");
        dlTimeNameLabel.setFont(defaultFont);
        dlTimeValueSlider = new JSlider(10, 5000, 1000);
        dlNumberNameLabel = new JLabel("Dealers Number:");
        dlNumberNameLabel.setFont(defaultFont);
        dlNumberValueText = new JTextField("3",5);
        dlNumberValueText.setFont(defaultFont);

        okBtn = new JButton("OK");
        cancelBtn = new JButton("Cancel");
        okBtn.setPreferredSize(Factory.BUTTONS_SIZE);
        cancelBtn.setPreferredSize(Factory.BUTTONS_SIZE);
    }

    private void createUI() {
        mainContainer.setLayout(new BorderLayout());
        mainContainer.add(infoPanel, BorderLayout.CENTER);
        mainContainer.add(buttonsPanel, BorderLayout.SOUTH);

        //Line 1
        infoPanel.add(esNameLabel);
        infoPanel.add(esTimeValueLabel);

        infoPanel.add(wNameLabel);
        infoPanel.add(wTimeValueLabel);

        //Line 2
        infoPanel.add(esTimeNameLabel);
        infoPanel.add(esTimeValueSlider);

        infoPanel.add(wTimeNameLabel);
        infoPanel.add(wTimeValueSlider);

        //Line 3
        infoPanel.add(bsNameLabel);
        infoPanel.add(bsTimeValueLabel);

        infoPanel.add(wNumberNameLabel);
        infoPanel.add(wNumberValueText);

        //Line 4
        infoPanel.add(bsTimeNameLabel);
        infoPanel.add(bsTimeValueSlider);

        infoPanel.add(dlNameLabel);
        infoPanel.add(dlTimeValueLabel);

        //Line 5
        infoPanel.add(asNameLabel);
        infoPanel.add(asTimeValueLabel);

        infoPanel.add(dlTimeNameLabel);
        infoPanel.add(dlTimeValueSlider);

        //Line 6
        infoPanel.add(asTimeNameLabel);
        infoPanel.add(asTimeValueSlider);

        infoPanel.add(dlNumberNameLabel);
        infoPanel.add(dlNumberValueText);

        //Line 7
        infoPanel.add(asNumberNameLabel);
        infoPanel.add(asNumberValueText);

        infoPanel.add(new JLabel(""));
        infoPanel.add(new JLabel(""));

        buttonsPanel.add(okBtn);
        buttonsPanel.add(cancelBtn);

        setSize(Factory.SETTINGS_DIALOG_SIZE);
        setLocationRelativeTo(owner);
        setResizable(false);
        setVisible(false);
        pack();
    }

    private void initListeners() {
        okBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                applyChanges(true);
            }
        });

        okBtn.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) applyChanges(true);
            }
        });

        cancelBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                applyChanges(false);
            }
        });

        cancelBtn.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) applyChanges(false);
            }
        });
        esTimeValueSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                esTimeValueLabel.setText(String.valueOf(value));
            }
        });
        bsTimeValueSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                bsTimeValueLabel.setText(String.valueOf(value));
            }
        });
        asTimeValueSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                asTimeValueLabel.setText(String.valueOf(value));
            }
        });
        wTimeValueSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                wTimeValueLabel.setText(String.valueOf(value));
            }
        });
        dlTimeValueSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                dlTimeValueLabel.setText(String.valueOf(value));
            }
        });
        asNumberValueText.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            }
            public void focusLost(FocusEvent e) {
                try {
                    int value = Integer.parseInt(asNumberValueText.getText());
                } catch (NumberFormatException ex) {
                    asNumberValueText.setText("1");
                }
            }
        });
        dlNumberValueText.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            }
            public void focusLost(FocusEvent e) {
                try {
                    int value = Integer.parseInt(dlNumberValueText.getText());
                } catch (NumberFormatException ex) {
                    dlNumberValueText.setText("1");
                }
            }
        });
    }

    public SettingsDialog(JFrame owner, Factory f) {
        super(owner, f.SETTINGS_DIALOG_HEAD, true);
        this.owner = owner;
        this.factory = f;
        initElements();
        initListeners();
        createUI();
    }

    private void applyChanges(boolean yes) {
        factory.updateSettings(yes);
        setVisible(false);
    }

    public void setESTimeValue(int value) { esTimeValueSlider.setValue(value); }

    public void setBSTimeValue(int value) { bsTimeValueSlider.setValue(value); }

    public void setASTimeValue(int value) { asTimeValueSlider.setValue(value); }

    public void setASNumberValue(int value) { asNumberValueText.setText(String.valueOf(value)); }

    public void setWTimeValue(int value) { wTimeValueSlider.setValue(value); }

    public void setWNumberValue(int value) { wNumberValueText.setText(String.valueOf(value)); }

    public void setDLTimeValue(int value) { dlTimeValueSlider.setValue(value); }

    public void setDLNumberValue(int value) { dlNumberValueText.setText(String.valueOf(value)); }

    public int getESTimeValue() { return esTimeValueSlider.getValue(); }

    public int getBSTimeValue() { return bsTimeValueSlider.getValue(); }

    public int getASTimeValue() { return asTimeValueSlider.getValue(); }

    public int getASNumberValue() { return Integer.parseInt(asNumberValueText.getText()); }

    public int getWTimeValue() { return wTimeValueSlider.getValue(); }

    public int getWNumberValue() { return Integer.parseInt(wNumberValueText.getText()); }

    public int getDLTimeValue() { return dlTimeValueSlider.getValue(); }

    public int getDLNumberValue() { return Integer.parseInt(dlNumberValueText.getText()); }
}
