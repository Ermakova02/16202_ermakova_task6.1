package ru.nsu.ccfit.ermakova.threadpool;

import java.util.logging.Logger;

public abstract class Operator implements Runnable {
    private static Logger log = Logger.getLogger(Operator.class.getName());

    private int delay;
    protected boolean started;
    protected boolean stopped;
    protected boolean paused;
    protected Storage stg;
    protected String name;
    protected int count;
    public Thread t;

    public Operator(String strName) {
        delay = 0;
        started = false;
        stopped = false;
        paused = false;
        stg = null;
        name = strName;
        count = 0;
        t = new Thread(this, name);
        t.start();
    }

    public void setDelay(int delay) { this.delay = delay; }

    public void setStorage(Storage stg) { this.stg = stg; }

    public void setName(String name) { this.name = name; }

    public int getCount() { return count; }

    protected abstract void next() throws InterruptedException;

    public synchronized void start() {
        started = true;
        paused = false;
        stopped = false;
        notifyAll();
    }

    public synchronized void stop() {
        started = true;
        paused = false;
        stopped = true;
        t.interrupt();
        notifyAll();
    }

    public synchronized void pause() {
        paused = true;
        notifyAll();
    }

    public synchronized void resume() {
        paused = false;
        notifyAll();
    }

    @Override
    public void run() {
        while (!t.isInterrupted())
        {
            try {
                synchronized (this) {
                    while (!started || paused) {
                        if (t.isInterrupted()) throw new InterruptedException();
                        wait();
                    }
                }
            } catch (InterruptedException e) {
                return;
            }
            if (stopped) break;
            if (paused) continue;
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                return;
            }
            if (stopped) break;
            if (paused) continue;
            try {
                next();
            } catch (InterruptedException e) {
                return;
            }
        }
    }
}
