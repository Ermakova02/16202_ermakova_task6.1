package ru.nsu.ccfit.ermakova.threadpool;

import java.util.ArrayDeque;

public class Storage<T> {
    private int size;
    private int q_size;
    private ArrayDeque<T> queue;
    private String name;
    private boolean readyForOperation;

    public Storage() {
        size = 0;
        q_size = 0;
        queue = null;
        name = null;
        readyForOperation = true;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() { return size; }

    public int getElementsCount() { return q_size; }

    public synchronized void setSize(int size) throws InterruptedException {
        while (!readyForOperation) {
            wait();
        }
        readyForOperation = false;
        this.size = size;
        queue = new ArrayDeque<T>(size);
        q_size = queue.size();
        readyForOperation = true;
        notifyAll();
    }

    public synchronized void put(T p) throws InterruptedException {
        while (!readyForOperation || q_size >= size) {
            wait();
        }
        readyForOperation = false;
        queue.addLast(p);
        q_size = queue.size();
        readyForOperation = true;
        notifyAll();
    }

    public synchronized T get() throws InterruptedException {
        while (!readyForOperation || q_size == 0) {
            wait();
        }
        readyForOperation = false;
        T p = queue.remove();
        q_size = queue.size();
        readyForOperation = true;
        notifyAll();
        return p;
    }
}
