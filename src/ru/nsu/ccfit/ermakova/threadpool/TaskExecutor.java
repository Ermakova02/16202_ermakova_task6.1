package ru.nsu.ccfit.ermakova.threadpool;

public class TaskExecutor implements Runnable {
    PoolBlockingQueue<Runnable> queue;

    public TaskExecutor(PoolBlockingQueue<Runnable> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            while (true) {
                String name = Thread.currentThread().getName();
                Runnable task = queue.dequeue();
                task.run();
            }
        } catch (InterruptedException e) {
//            e.printStackTrace();
        }

    }
}
