package ru.nsu.ccfit.ermakova.threadpool;

public class ThreadPool {
    private PoolBlockingQueue <Runnable> queue;
    private Thread[] tasks;
    private int nThread;
    public ThreadPool(int queueSize, int nThread) {
        queue = new PoolBlockingQueue<>(queueSize);
        this.nThread = nThread;
        String threadName = null;
        TaskExecutor task = null;
        tasks = new Thread[nThread];
        for (int i = 0; i < nThread; i++) {
            threadName = "Thread-" + i;
            task = new TaskExecutor(queue);
            tasks[i] = new Thread(task, threadName);
            tasks[i].start();
        }
    }
    public void shutdown() {
        if (tasks != null)
            for (int i = 0; i < nThread; i++) {
                if (tasks[i] != null) {
                    tasks[i].interrupt();
                    try {
                        tasks[i].join(100);
                    } catch (InterruptedException e) {
                    }
                }
            }
        queue = null;
        tasks = null;
    }
    public int waitingTasksCount() {
        return queue == null ? 0 : queue.size();
    }
    public void submitTask(Runnable task) throws InterruptedException {
        queue.enqueue(task);
    }
}
